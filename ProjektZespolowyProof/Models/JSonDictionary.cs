using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

namespace ProjektZespolowyProof.Models
{
    class JSonDictionary
    {
        protected delegate bool Insert(int id,JToken token);

        //Adds contents of file to collection
        protected static bool LoadData(String path, Insert insert)
        {
            int id = -1;
            JObject List = null;

            try{ List = JObject.Parse(File.ReadAllText(path));}
            catch(JsonReaderException e){ Console.WriteLine("json parse exception with "+path+" file:\n"+e.Message); }
            catch(Exception e){ Console.WriteLine("io exception with "+path+" file:\n"+e.Message); }
            if(List==null) return false;

            foreach(var l in List)
                try
                { 
                    if(!Int32.TryParse(l.Key, out id)|| !insert.Invoke(id, l.Value)) 
                        throw new Exception();
                } 
                catch(Exception e) {Console.WriteLine("reject "+id+"\n"+e.Message);}

            return true;
        }

        //Write text to file
        protected static bool SaveData(String path, String text)
        {
            try{ File.WriteAllText(path, text); }//JsonConvert.SerializeObject(Collection, Formatting.Indented)
            catch(Exception e){ Console.WriteLine(" an error found with writing "+path+" file:\n"+e.Message); return false; }
            return true;
        }
    }
}