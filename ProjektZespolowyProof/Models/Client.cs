using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ProjektZespolowyProof.Models
{
    public class Client
    {
        [JsonProperty("id")] 
        public int id;
        [JsonProperty("firstName")] 
        public String firstName;
        [JsonProperty("lastName")] 
        public String lastName;
        [JsonProperty("email")] 
        public String email;
        [JsonProperty("phone")] 
        public String phone;
        [JsonProperty("street")] 
        public String street;
        [JsonProperty("postalCode")] 
        public String postalCode;
        [JsonProperty("city")] 
        public String city;
        [JsonProperty("contactPerson")] 
        public String contactPerson;

        [JsonProperty("services")] 
        public Dictionary<int,ServiceDetails> services;


        public int AddService(ServiceDetails s)
        {
            int id = 0;
            while(services.ContainsKey(id)) id++;
            s.id = id;
            services.Add(id, s);

            return AllClients.SaveClients()? id:-1;//can't write file
        }
        public int UpdateService(int id, ServiceDetails s)
        {
            if(!services.ContainsKey(id)) return -1;
            ServiceDetails serv = services[id];
            if(s.creationDate.Year!=1) serv.creationDate = s.creationDate;
            if(s.expirationDate.Year!=1) serv.expirationDate = s.expirationDate;

            return AllClients.SaveClients()? id:-2;  
        }
        public int DeleteService(int id)
        {
            if(!services.ContainsKey(id)) return -1;
            services.Remove(id);
            
            return AllClients.SaveClients()? 1:-1;
        }
    }
}