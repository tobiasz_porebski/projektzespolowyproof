using System;
using Newtonsoft.Json;

namespace ProjektZespolowyProof.Models
{
    public class ServiceDetails
    {
        [JsonProperty("id")]
        public int id;
        [JsonProperty("creationDate")]
        public DateTime creationDate;
        [JsonProperty("expirationDate")]
        public DateTime expirationDate;
    }
}