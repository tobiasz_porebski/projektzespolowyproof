using System;
using System.Collections;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

namespace ProjektZespolowyProof.Models
{
    class AllServices : JSonDictionary
    {
        public static String FilePath = @"..\data\ServiceList.json";
        public static Dictionary<int,Service> Collection = new Dictionary<int,Service>();

        private static bool Converter(int id, JToken t)
        {
            Collection.Add(id,t.ToObject<Service>());
            return true;
        }

        public static bool LoadServices()
        {
            Collection.Clear();
            return LoadData(FilePath, Converter);
        }

        public static bool SaveServices()
        {
            try{ SaveData(FilePath, JsonConvert.SerializeObject(Collection, Formatting.Indented)); }
            catch(Exception e){ Console.WriteLine(" an error found with writing "+FilePath+" file:\n"+e.Message); return false; }
            return true;
        }
    }
}