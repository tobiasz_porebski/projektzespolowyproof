using System;
using System.Collections;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

namespace ProjektZespolowyProof.Models
{
    class AllClients : JSonDictionary
    {
        static String FilePath = @"..\data\ClientList.json";
        public static Dictionary<int,Client> Collection = new Dictionary<int,Client>();

        // returns dictionary with clients without details and services list
        public static SCList GetShortAll(int amount, int offset)
        {
            SCList sclist = new SCList();
            sclist.List = new Dictionary<int, ShortClient>();
            foreach(var l in Collection) 
            {
                if(offset-->0) continue;
                if(amount-->0) { sclist.List.Add(l.Key, new ShortClient(l.Value)); continue; }
                break;
            } 
            sclist.Count = Collection.Count;
            return sclist;
        }

        private static bool Converter(int id, JToken t)
        {
            Client c = t.ToObject<Client>();
            c.id = id;
            if(c.services == null) c.services = new Dictionary<int, ServiceDetails>();
            Collection.Add(id,c);
            return true;
        }

        // read content of file with list (ClientList.json)
        //  returns false when any error appears (IO, JSonArray.Parse)
        public static bool LoadClients()
        {
            Collection.Clear();
            return LoadData(FilePath, Converter);
        }

        // writes actual state of collection to file (ClientList.json)
        public static bool SaveClients()
        {
            try{ SaveData(FilePath, JsonConvert.SerializeObject(Collection, Formatting.Indented)); }
            catch(Exception e){ Console.WriteLine(" an error found with writing "+FilePath+" file:\n"+e.Message); return false; }
            return true;
        }
        // adds new client to collection and updates file
        //  returns id of created client, -1 in case of any error
        public static int AddClient(Client cl, bool commit_changes = true)
        {
            int id=0;
            while(Collection.ContainsKey(id)) id++;
            Collection.Add(id, cl);
            
            if(!commit_changes) return id;
                else return SaveClients()? id:-1;
        }

        public static int UpdateClient(int id, Client cl, bool commit_changes = true)
        {
            if(!Collection.ContainsKey(id)) return -1;
            Client client = Collection[id];
            if(cl.firstName!=null) client.firstName = cl.firstName;
            if(cl.lastName!=null) client.lastName = cl.lastName;
            if(cl.email!=null) client.email = cl.email;
            if(cl.phone!=null) client.phone = cl.phone;
            if(cl.street!=null) client.street = cl.street;
            if(cl.postalCode!=null) client.postalCode = cl.postalCode;
            if(cl.city!=null) client.city = cl.city;
            if(cl.email!=null) client.email = cl.email;
            if(cl.contactPerson!=null) client.contactPerson = cl.contactPerson;

            if(!commit_changes) return 1;// ok
                else return SaveClients()? id:-2;  
        }

        public static int DeleteClient(int id, bool commit_changes = true)
        {
            if(!Collection.ContainsKey(id)) return -1;
            Collection.Remove(id);
            if(!commit_changes) return 1;
                else return SaveClients()? id:-1;
        }
    }
}