using System;
using Newtonsoft.Json;

namespace ProjektZespolowyProof.Models
{
    public class Service
    {
        [JsonProperty("id")]
        public int id;
        [JsonProperty("name")]
        public String name {get;set;}
        [JsonProperty("description")]
        public String description;
    }
}