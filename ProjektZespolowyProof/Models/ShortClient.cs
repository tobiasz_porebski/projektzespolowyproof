using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ProjektZespolowyProof.Models
{
    public class SCList
    {
        [JsonProperty("list")] 
        public Dictionary<int, ShortClient> List;
        [JsonProperty("count")] 
        public int Count;
    }

    public class ShortClient
    {
        [JsonProperty("id")] 
        public int id;
        [JsonProperty("firstName")] 
        public String firstName;
        [JsonProperty("lastName")] 
        public String lastName;
        [JsonProperty("email")] 
		public String email;
        [JsonProperty("amountServices")] 
		public int amountServices;
        
        public ShortClient(Client c){
            id = c.id;
            firstName = c.firstName;
            lastName = c.lastName;
            email = c.email;
            amountServices = c.services.Count;
        }
    }
}