﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using ProjektZespolowyProof.Models;

namespace ProjektZespolowyProof
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //Console.WriteLine(Directory.GetParent(Directory.GetCurrentDirectory()).ToString());
            Console.WriteLine("Loaded clients: "+AllClients.LoadClients());
            Console.WriteLine("Loaded services: "+AllServices.LoadServices());
            Console.WriteLine();

            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseWebRoot(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).ToString(),"wwwroot"))
                .Build();
    }
}
