using Microsoft.AspNetCore.Mvc;
using System;
using ProjektZespolowyProof.Models;

namespace ProjektZespolowyProof.Api
{
    [Produces("application/json")]
    [Route("api/services")]
    public class ServicesSchemeController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(AllServices.Collection);
        }
    }

    [Produces("application/json")]// enable converting response to json format (allows casting to c# objects)
    [Route("api/clients/{client_id}/services")]// urls to catch
    public class ServicesController : Controller
    {
        /// <summary> Get (without parameters) [Gets all clients] </summary>
        /// <remarks> URL: /api/clients </remarks>
        /// <returns> Object with list </returns>
        /// <response code="200"> JSon list object </response>
        /// <response code="404"> String </response>
        [HttpGet]
        public IActionResult Get(int client_id)
        {
            if(!AllClients.Collection.ContainsKey(client_id))
                return NotFound("no client with such id ["+client_id+"]");
            return Ok(AllClients.Collection[client_id].services);
        }

        /// <summary> Get (with parameters) [Gets specific client] </summary>
        /// <remarks> URL: /api/clients/{id} </remarks>
        /// <param name="id"> Client id </param>
        /// <returns> Client object </returns>
        /// <response code="200"> JSon Client obj  </response>
        /// <response code="404"> String </response>
        [HttpGet("{service_id}")]
        [ProducesResponseType(typeof(Client), 200)]
        [ProducesResponseType(typeof(String), 404)]
        public IActionResult Get(int client_id, int service_id)
        {
            if(!AllClients.Collection.ContainsKey(client_id))
                return NotFound("no client with such id ["+client_id+"]");
            if(!AllClients.Collection[client_id].services.ContainsKey(service_id))
                return NotFound("no service with such id ["+service_id+"]");
            return Ok(AllClients.Collection[client_id].services[service_id]);
        }

        // Note: POST is for creating new objects. It should return http code 201 (Created) if successful.
        // Good practice is to return path and ID of newly created element in the response body - using: Created($"{Request.Path}{newAppID}", ResultMessage.CreateForObjectCreated(newAppID))
        /// <summary> Adds new sample application. </summary>
        /// <remarks> 
        /// Sample path (POST) 
        /// Provide application data in the message body
        ///     URL: /api/clients
        ///     BODY:
        ///     {
        ///         "name": "Vincent",
        ///         "surname": "Russo"
        ///     }
        /// </remarks>
        /// <param name="newClient"> Application object </param>
        /// <returns> ID of the newly created client </returns>
        /// <response code="201"> Returns the status message containing id of the newly created client.</response>
        /// <response code="500"> When cannot save changes </response>
        [HttpPost]
        [ProducesResponseType(typeof(String), 201)]
        [ProducesResponseType(typeof(String), 404)]
        [ProducesResponseType(500)]
        public IActionResult Post(int client_id, [FromBody]ServiceDetails newService)
        {
            if(!AllClients.Collection.ContainsKey(client_id))
                return NotFound("no client with such id ["+client_id+"]");
            Client c = AllClients.Collection[client_id];
            int id = c.AddService(newService);
            c.services[id].id = id;
            if(id<0) return StatusCode(500);
            return Created($"{Request.Path}/{id}", "created new service");
        }

        // Note: PUT is for updating existing objects. It should return http code 200 if successful.
        // If element does not exist please return 404 code with proper result message - best to use: NotFound(ResultMessage.CreateForNotFound())
        /// <summary> Updates indicated sample application </summary>
        /// <remarks>
        /// Provide application database identifier in the path and application data in the body
        ///     URL: /api/clients/{id}
        ///     BODY:
        ///     {
        ///         "surname": "blablabla"
        ///     }
        /// </remarks>
        /// <param name="id"> ID of client to update</param>
        /// <param name="client"> Client obj </param>
        /// <returns> Nothing </returns>
        /// <response code="200"> When the operation was successful </response>
        /// <response code="404"> If the client with provided id doesn't exist </response>
        /// <response code="500"> When backend cannot save changes </response>
        [HttpPut("{service_id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(String), 404)]
        [ProducesResponseType(500)]
        public IActionResult Put(int client_id, int service_id, [FromBody]ServiceDetails service)
        {
            if(!AllClients.Collection.ContainsKey(client_id))
                return NotFound("no client with such id ["+client_id+"]");

            Client c = AllClients.Collection[client_id];
            int res = c.UpdateService(service_id, service);
            if(res<0) return StatusCode(500);//server error
            return Ok();
        }


        // Note: DELETE is for deleting existing objects. It should return http code 204 (NoContent) if successful.
        // If element does not exist please return 404 (NotFound) code with proper result message - best to use: NotFound(ResultMessage.CreateForNotFound())
        /// <summary> Deletes indicated client </summary>
        /// <remarks>
        /// Provide application database identifier in the path (DELETE)
        ///     URL: /api/clients/5  
        /// </remarks>
        /// <param name="id"> ID of a client to delete. </param>
        /// <returns> Status </returns>
        /// <response code="204"> When the operation was successful. </response>
        /// <response code="404"> If the client with provided id doesn't exist. </response>
        /// <response code="500"> When cannot save changes </response>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(String), 404)]
        [ProducesResponseType(typeof(String), 500)]
        public IActionResult Delete(int client_id, int id)
        {
            if(!AllClients.Collection.ContainsKey(client_id))
                return NotFound("no client with such id ["+client_id+"]");

            Client c = AllClients.Collection[client_id];
            int res = c.DeleteService(id);
            if(res == -1) return NotFound("no service with id "+id);
            if(res < 0) return StatusCode(500);
            return NoContent();
        }

        // Note: Error method is for testing and presentation purposes only. It always throws unhandled server side exception.
        /// <summary>
        /// Throws unhandled exception on the server side.
        /// </summary>
        /// <returns>Nothing (the method always throws exception).</returns>
        [HttpGet("error")]
        [ProducesResponseType(500)]
        public IActionResult Error()
        {
            Console.WriteLine("throwed error in services");
            throw new Exception(); // ArgumentOutOfRangeException
        }
    }
}
