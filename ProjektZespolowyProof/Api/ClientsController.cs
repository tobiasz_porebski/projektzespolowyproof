﻿using Microsoft.AspNetCore.Mvc;
using ProjektZespolowyProof.Models;

using System;
//using System.Web.HttpGet;
using System.Collections;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

namespace ProjektZespolowyProof.Api
{
    [Produces("application/json")]// enable converting response to json format (allows casting to c# objects)
    //[ApiExplorerSettings(GroupName = "app")]
    [Route("api/clients")]// urls to catch
    public class AppContoller : Controller
    {
        /// <summary> Get (without parameters(only after ?)) [Gets all clients] </summary>
        /// <remarks> URL: /api/clients or /api/clients?a=4&p=1 [a - amount, p - page] </remarks>
        /// <returns> Object with list </returns>
        /// <response code="200"> JSon list object </response>
        [HttpGet]
        public IActionResult Get([FromQuery]int a=5, [FromQuery]int p=0)
        {
            return Ok(AllClients.GetShortAll(a,p*a));
        }

        /// <summary> Get (with parameters) [Gets specific client] </summary>
        /// <remarks> URL: /api/clients/{id} </remarks>
        /// <param name="id"> Client id </param>
        /// <returns> Client object </returns>
        /// <response code="200"> JSon Client obj  </response>
        /// <response code="404"> String </response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Client), 200)]
        [ProducesResponseType(typeof(String), 404)]
        public IActionResult Get(int id)
        {
            if(!AllClients.Collection.ContainsKey(id))
                return NotFound("no client with such id ["+id+"]");

            JObject obj = JObject.FromObject(AllClients.Collection[id]);
            obj["services"].Parent.Remove();
            return Ok(obj);
        }

        // Note: POST is for creating new objects. It should return http code 201 (Created) if successful.
        // Good practice is to return path and ID of newly created element in the response body - using: Created($"{Request.Path}{newAppID}", ResultMessage.CreateForObjectCreated(newAppID))
        /// <summary> Adds new sample application. </summary>
        /// <remarks> 
        /// Sample path (POST) 
        /// Provide application data in the message body
        ///     URL: /api/clients
        ///     BODY:
        ///     {
        ///         "name": "Vincent",
        ///         "surname": "Russo"
        ///     }
        /// </remarks>
        /// <param name="newClient"> Application object </param>
        /// <returns> ID of the newly created client </returns>
        /// <response code="201"> Returns the status message containing id of the newly created client.</response>
        /// <response code="500"> When cannot save changes </response>
        [HttpPost]
        [ProducesResponseType(typeof(String), 201)]
        [ProducesResponseType(500)]
        public IActionResult Post([FromBody]Client newClient)
        {
            int id = AllClients.AddClient(newClient);
            AllClients.Collection[id].id = id;
            AllClients.Collection[id].services = new Dictionary<int,ServiceDetails>();
            if(id<0) return StatusCode(500);
            return Created($"{Request.Path}/{id}", "created new client");
        }

        // Note: PUT is for updating existing objects. It should return http code 200 if successful.
        // If element does not exist please return 404 code with proper result message - best to use: NotFound(ResultMessage.CreateForNotFound())
        /// <summary> Updates indicated sample application </summary>
        /// <remarks>
        /// Provide application database identifier in the path and application data in the body
        ///     URL: /api/clients/{id}
        ///     BODY:
        ///     {
        ///         "surname": "blablabla"
        ///     }
        /// </remarks>
        /// <param name="id"> ID of client to update</param>
        /// <param name="client"> Client obj </param>
        /// <returns> Nothing </returns>
        /// <response code="200"> When the operation was successful </response>
        /// <response code="404"> If the client with provided id doesn't exist </response>
        /// <response code="500"> When backend cannot save changes </response>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(String), 404)]
        [ProducesResponseType(500)]
        public IActionResult Put(int id, [FromBody]Client client)
        {
            int res = AllClients.UpdateClient(id,client);
            if(res==-1) return NotFound("no client with id "+id);
            if(res<0) return StatusCode(500);//server error
            return Ok();
        }


        // Note: DELETE is for deleting existing objects. It should return http code 204 (NoContent) if successful.
        // If element does not exist please return 404 (NotFound) code with proper result message - best to use: NotFound(ResultMessage.CreateForNotFound())
        /// <summary> Deletes indicated client </summary>
        /// <remarks>
        /// Provide application database identifier in the path (DELETE)
        ///     URL: /api/clients/5  
        /// </remarks>
        /// <param name="id"> ID of a client to delete. </param>
        /// <returns> Status </returns>
        /// <response code="204"> When the operation was successful. </response>
        /// <response code="404"> If the client with provided id doesn't exist. </response>
        /// <response code="500"> When cannot save changes </response>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(String), 404)]
        [ProducesResponseType(typeof(String), 500)]
        public IActionResult Delete(int id)
        {
            int res = AllClients.DeleteClient(id);
            
            if(res==-1) return NotFound("no client with id "+id);
            if(res<0) return StatusCode(500);
            return NoContent();
        }

        // Note: Error method is for testing and presentation purposes only. It always throws unhandled server side exception.
        /// <summary>
        /// Throws unhandled exception on the server side.
        /// </summary>
        /// <returns>Nothing (the method always throws exception).</returns>
        [HttpGet("error")]
        [ProducesResponseType(500)]
        public IActionResult Error()
        {
            throw new Exception(); // ArgumentOutOfRangeException
        }
    }
}
