import React, {Component} from 'react'


class fetchTest extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true
        }
    }

    componentDidMount() {
        fetch("http://localhost:5000/api/clients")
            .then((response) => response.json())
            .then(response => console.log(response))
            .then(this.setState({isLoading: false}))
            .catch(error => console.log(error));

        /*fetch("http://localhost:5000/api/clients", {
            method: 'POST',
            headers: new Headers({'Content-Type': 'application/json'}),
            body: JSON.stringify({
                firstName: "Rafal",
                lastName: "Pelka",
                email: "test@test.com",
                phone: "553241513",
                street: "test street",
                postalCode: '32-532',
                city: "Krakow",
                contactPerson: "Test Person"
            })
        })
            .then(function (data) {
                console.log('Request success: ', data);
            })
            .catch(function (error) {
                console.log('Request failure: ', error);
            });*/
        /*var prom = new Promise(function (resolve, reject) {
            resolve("Stuff worked!");
        });

        prom.then(function (result) {
            console.log(result);
        });*/
    }

    render() {
        return (
            this.state.isLoading ?
                <p>Loading...</p> :
                <p>Loaded</p>
        )
    }
}

export default fetchTest;