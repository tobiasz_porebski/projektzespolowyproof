import React from 'react'
import PropTypes from 'prop-types'
import {ConnectedRouter} from 'connected-react-router'
import Content from './components/VisibleContent'

const App = ({history}) => {
    return (
        <ConnectedRouter history={history}>
            <Content/>
        </ConnectedRouter>
    )
}

App.propTypes = {
    history: PropTypes.object,
}

export default App
