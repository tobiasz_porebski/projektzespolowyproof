import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import {applyMiddleware, compose, createStore} from 'redux'
import {createBrowserHistory} from 'history'
import {AppContainer} from 'react-hot-loader'
import {routerMiddleware, connectRouter} from 'connected-react-router'
import App from './App'
import rootReducer from './reducers'
import thunk from 'redux-thunk'
import Promise from 'promise-polyfill'
import 'semantic-ui-css/semantic.min.css';

import './../node_modules/bootstrap/dist/css/bootstrap.css'
import './../src/public/css/style.css'
import './../src/public/text.css'

if (!window.Promise) {
    window.Promise = Promise;
}

const history = createBrowserHistory()

const initialState = {
    navBar: {
        homeClassName: "",
        clientsClassName: ""
    }
};

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
    connectRouter(history)(rootReducer),
    initialState,
    composeEnhancer(
        applyMiddleware(
            routerMiddleware(history),
            thunk
        ),
    ),
)

const render = () => {
    ReactDOM.render(
        <AppContainer>
            <Provider store={store}>
                <App history={history}/>
            </Provider>
        </AppContainer>,
        document.getElementById('wrapper')
    )
}

render()

if (module.hot) {
    module.hot.accept('./App', () => {
        render()
    })

    module.hot.accept('./reducers', () => {
        store.replaceReducer(connectRouter(history)(rootReducer))
    })
}
