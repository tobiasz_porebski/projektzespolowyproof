import actionTypes from './actionTypes';


const updateClientAction = (newData) => {
    return {
        type: actionTypes.UPDATE_CLIENT,
        newData: newData
    }
}

const updateClient = (id, newData) => {
    return dispatch => {
        fetch(("http://localhost:5000/api/clients/"+id), {
            method: 'put',
            headers: new Headers({'Content-Type': 'application/json'}),
            body: JSON.stringify(newData)
        })
            .then(response => {
                console.log(response)
                dispatch(updateClient(id, newData))
            })
    }
}

export default updateClient;