import actionTypes from './actionTypes';


const displayClientsList = (data) => {
    return {
        type: actionTypes.DISPLAY_CLIENTS_LIST,
        clientsData: data.list,
        count: data.count,
    }
}

const loadClients = (page, amountOnThePage) => {
    const params = {p: page, a: amountOnThePage};
    let url = "http://localhost:5000/api/clients";
    let query = Object.keys(params)
        .filter(k => params[k] !== undefined)
        .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
        .join('&');
    url += (url.indexOf('?') === -1 ? '?' : '&') + query;
    return (dispatch) => {
        fetch(url)
            .then(response => response.json())
            .then(response => {
                console.log(response);
                return response;
            })
            .then(response => dispatch(displayClientsList(response)))
            .catch(error => console.log(error))
    }
}

export default loadClients;

