import actionTypes from './actionTypes';


const createDeleteAction = (id) => {
    return {
        type: actionTypes.DELETE_CLIENT,
        id: id
    }
}

const deleteClient = (id) => {
    return (dispatch) => {
        fetch('http://localhost:5000/api/clients/'+id, {
            method: 'delete'
        })
            .then(dispatch(createDeleteAction(id)))
    }
}

export default deleteClient;