import actionTypes from './actionTypes';
import loadClients from './clientsList';
import {hideModal} from './modal';

export const addClient = (data) => {
    return {
        type: actionTypes.ADD_NEW_CLIENT,
        data: data
    };
};

export const putNewClient = (data) => {
    return dispatch => {
        console.log('post', data)
        fetch("http://localhost:5000/api/clients", {
            method: 'POST',
            headers: new Headers({'Content-Type': 'application/json'}),
            body: JSON.stringify(data)
        })
            .then(() => {
                dispatch(addClient(data))
            })
            .then(() => {
                loadClients()
            })
            .then(() => {
                dispatch(hideModal())
            })
            .catch(function (error) {
                console.log('Request failure: ', error);
            });
    };
};