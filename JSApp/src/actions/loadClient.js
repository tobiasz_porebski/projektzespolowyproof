import actionTypes from './actionTypes';
import {showModal} from './modal';

const loadClientAction = (data) => {
    return {
        type: actionTypes.LOAD_CLIENT,
        data: data
    }
}


const loadClients = (id) => {
    return dispatch => {
        fetch('http://localhost:5000/api/clients/' + id)
            .then(response => response.json())
            .then(json => {
                console.log(json);
                dispatch(loadClientAction(json));
            })
            .then(json => dispatch(showModal('MODAL_TYPE_EDIT_CLIENT', {})))
            .catch(error => console.log(error))
    }
}

export default loadClients;