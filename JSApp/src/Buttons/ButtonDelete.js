import React from "react";

class ButtonDelete extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <button onClick={this.props.onClick} type="button" className="btn btn-warning buttonsEditManageAndDelete">
                <i className="fa fa-warning"></i>
                <span className="bold">Delete</span>
            </button>
        )
    }
}

export default ButtonDelete