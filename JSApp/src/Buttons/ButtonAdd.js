import React from "react";

export default class ButtonAdd extends React.Component {

    render() {
        return (
            <button onClick={this.props.onClick}
                type="button" className="btn btn-primary btn-lg bigButton">
                New Client
            </button>
        )
    }
}