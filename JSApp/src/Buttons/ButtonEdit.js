import React from "react";

class ButtonEdit extends React.Component {
    render() {
        return (
            <button onClick={this.props.onClick} type="button" className="btn btn-info buttonsEditManageAndDelete">
                <i className="fa fa-paste"></i>Edit
            </button>
        )
    }
}

export default ButtonEdit