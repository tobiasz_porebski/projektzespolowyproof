import React from "react";
import $ from "jquery";

import '../public/text.css'

class ButtonManage extends React.Component {
    render() {
        return (
            <button onClick={this.props.onClick} type="button" className="buttonsEditManageAndDelete btn-primary btn">
                <span className="bold">Manage</span>
            </button>
        )
    }
}

export default ButtonManage