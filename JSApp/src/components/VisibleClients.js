import React from 'react'
import {connect} from 'react-redux'
import Clients from "./ClientsList/ListClients";

function mapStateToProps(state) {
    return {
        pathname: state.router.location.pathname,
        search: state.router.location.search,
        hash: state.router.location.hash
    };
}

function mapDispatchToProps(dispatch) {
    return {
        clientsComponentDidMount: () => dispatch({type: 'CLIENTS_COMPONENT'})
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Clients)