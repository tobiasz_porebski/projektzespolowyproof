export const required = value => value ? undefined : 'Wymagane'
export const maxLength = max => value =>
    value && value.length > max ? `Musi być ${max} liter lub mniej` : undefined
export const maxLength15 = maxLength(15)
export const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
        'Niepoprawny adres email' : undefined
export const postalCode = value =>
    value && !/\d\d-\d\d\d/.test(value) ?
        'Niepoprawny kod pocztowy' : undefined