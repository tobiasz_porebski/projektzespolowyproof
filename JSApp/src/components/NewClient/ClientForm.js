import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';

import * as actionCreators from '../../actions/newClient';
import * as v from './validation';
import Cont from '../Cont';
import {hideModal} from '../../actions/modal';

const renderField = ({input, label, type, meta: {touched, error, warning}}) => (
    <div className="form-group">
        <div className="col-lg-10">
            <label>{label}</label>
            <div>
                <input className="form-control" {...input} placeholder={label} type={type}/>
                {touched && ((error && <span style={{color: "red"}}>{error}</span>) || (warning &&
                    <span>{warning}</span>))}
            </div>
        </div>
    </div>
)

const ClientForm = (props ) => {

        const {handleSubmit, pristine, reset, submitting} = props
        console.log("testsdfasdfasdfasdfs");
        return (

            <Cont>
                <div className="modal-header">
                    <h1 className="modal-title">Add new client</h1>
                </div>
                <div className="modal-body">
                    <form className="form-horizontal" onSubmit={handleSubmit}>

                        <Field name="firstName" type="text"
                               component={renderField} label="Imię"
                               validate={[v.required, v.maxLength15]}
                        />
                        <Field name="lastName" type="text"
                               component={renderField} label="Nazwisko"
                               validate={[v.required, v.maxLength15]}
                        />
                        <Field name="email" type="email"
                               component={renderField} label="Email"
                               validate={v.email}
                        />
                        <Field name="phone" type="textarea"
                               component={renderField} label="Telefon"
                        />
                        <Field name="street" type="text"
                               component={renderField} label="Ulica"
                               validate={[v.required, v.maxLength15]}
                        />
                        <Field name="postalCode" type="text"
                               component={renderField} label="Kod pocztowy"
                               validate={[v.required, v.postalCode]}
                        />
                        <Field name="city" type="text"
                               component={renderField} label="Miasto"
                               validate={[v.required, v.maxLength15]}
                        />
                        <Field name="contactPerson" type="text"
                               component={renderField} label="Osoba kontaktowa"
                               validate={[v.required, v.maxLength15]}
                        />
                        <div>
                            <button className="btn btn-primary btn-sm"
                                    style={{margin: "10px"}}
                                    type="submit"
                                    disabled={submitting}>Submit
                            </button>

                            <button className="btn btn-primary btn-sm"
                                    type="button"
                                    disabled={pristine || submitting}
                                    onClick={reset}>Clear Values
                            </button>
                        </div>
                    </form>
                </div>
            </Cont>



        )

}

const mapStateToProps = (state) => {
    return {
        showModal: state.clientsList.showNewClientModal,
        initialValues: state.clientsList.loadedClientData
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit: (submittedData) => dispatch(actionCreators.putNewClient(submittedData)),
        closeModalHandler: () => dispatch(hideModal())
    }
}

const formConnected = reduxForm({
    form: 'clientForm', // unikalny identyfikator
    enableReinitialize: true
})(ClientForm)

export default connect(mapStateToProps, mapDispatchToProps)(formConnected)

/*
 export default reduxForm({
 form: 'clientForm' // unikalny identyfikator
 })(Connected)*/
