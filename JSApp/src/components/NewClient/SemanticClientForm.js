import React from 'react';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';

import * as actionCreators from '../../actions/newClient';
import * as v from './validation';
import {Form, Segment, Header, Button, Label} from 'semantic-ui-react';
import {hideModal} from '../../actions/modal';
import Cont from '../Cont';
import updateClient from '../../actions/updateClient';

const renderField = ({input, icon, label, meta: {touched, error}}) => {

    return (
        <Cont>
            {touched && ((error && <Label size={'medium'} basic color='red' pointing={'below'}>{error}</Label> ))}
            <Form.Input {...input}
                        error={error && touched}
                        warning={false}
                        icon={icon}
                        iconPosition='left'
                        placeholder={label}
            />

        </Cont>)


    /*<div className="form-group">
        <div className="col-lg-10">
            <label>{label}</label>
            <div>
                <input className="form-control" {...input} placeholder={label} type={type}/>
                {touched && ((error && <span style={{color: "red"}}>{error}</span>) || (warning &&
                    <span>{warning}</span>))}
            </div>
        </div>
    </div>*/
}

const ClientForm = (props) => {
    const {handleSubmit, pristine, reset, submitting} = props

    return (
        <Segment textAlign={'center'} >
            <Header as='h1' color='teal' textAlign='center'>
                Register new client
            </Header>
            <Segment style={{width: '70%', margin: '0 auto'}} raised textAlign={'center'}>
                <Form warning={false} error onSubmit={handleSubmit} size='huge'>

                    <Field name="firstName" type="text" icon="user"
                           component={renderField} label="Imię"
                           validate={[v.required, v.maxLength15]}
                    />
                    <Field name="lastName" type="text" icon="user"
                           component={renderField} label="Nazwisko"
                           validate={[v.required, v.maxLength15]}
                    />
                    <Field name="email" type="email" icon="mail"
                           component={renderField} label="Email"
                           validate={[v.required, v.email]}
                    />
                    <Field name="phone" type="textarea" icon="phone"
                           component={renderField} label="Telefon"
                           validate={v.required}
                    />
                    <Field name="street" type="text" icon="building outline"
                           component={renderField} label="Ulica"
                           validate={[v.required, v.maxLength15]}
                    />
                    <Field name="postalCode" type="text" icon="building outline"
                           component={renderField} label="Kod pocztowy"
                           validate={[v.required, v.postalCode]}
                    />
                    <Field name="city" type="text" icon="building outline"
                           component={renderField} label="Miasto"
                           validate={[v.required, v.maxLength15]}
                    />
                    <Field name="contactPerson" type="text" icon='user circle outline'
                           component={renderField} label="Osoba kontaktowa"
                           validate={[v.required, v.maxLength15]}
                    />
                    <Button type='submit' size={'huge'} primary style={{marginRight: '15px'}}>Submit</Button>
                    <Button size={'huge'} secondary onClick={reset} >Clear Values</Button>
                </Form>
            </Segment>

        </Segment>



    )
}

const mapStateToProps = (state) => {
    return {
        showModal: state.clientsList.showNewClientModal,
        initialValues: state.clientsList.loadedClientData
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit: (submittedData) => dispatch(actionCreators.putNewClient(submittedData)),
        closeModalHandler: () => dispatch(hideModal()),
        updateClientData: (id, newData) => dispatch(updateClient(id, newData))
    }
}

const formConnected = reduxForm({
    form: 'clientForm', // unikalny identyfikator
    enableReinitialize: true
})(ClientForm)

export default connect(mapStateToProps, mapDispatchToProps)(formConnected)

/*
 export default reduxForm({
 form: 'clientForm' // unikalny identyfikator
 })(Connected)*/
