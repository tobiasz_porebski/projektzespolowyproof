import React from 'react';
import {connect} from 'react-redux';

import {MODAL_TYPE_NOTIFICATION} from '../../constants/ModalTypes';
import {MODAL_TYPE_NEW_CLIENT} from '../../constants/ModalTypes';
import {MODAL_TYPE_EDIT_CLIENT} from "../../constants/ModalTypes";

import ModalNotification from './ModalNotification';
import ModalNewClient from './ModalNewClient';
import ModalEditClient from './ModalEditClient';


const MODAL_COMPONENTS = {
    [MODAL_TYPE_NOTIFICATION]: ModalNotification,
    [MODAL_TYPE_NEW_CLIENT]: ModalNewClient,
    [MODAL_TYPE_EDIT_CLIENT]: ModalEditClient
};

const ModalContainer = ({type, props}) => {
    if (!type) {
        return null;
    }

    const ModalComponent = MODAL_COMPONENTS[type];
    return <ModalComponent {...props} />;
};

export default connect(state => state.modal)(ModalContainer);