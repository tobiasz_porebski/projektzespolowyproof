import React from 'react';
import {connect} from 'react-redux';

import {hideModal} from '../../actions/modal';
import Modal from './Modal';
import ClientForm from '../NewClient/SemanticClientForm';

const ModalNewClient = ({afterClose, hideModal}) => {
    const onClose = () => {
        hideModal();

        if (afterClose) {
            afterClose();
        }
    };

    return (
        <Modal onClose={onClose}>
            <ClientForm loadClient={true}/>
        </Modal>
    );
};

export default connect(null, {hideModal})(ModalNewClient);
