import React from 'react';
import {connect} from 'react-redux';

import {hideModal} from '../../actions/modal';
import Modal from './Modal';

const ModalNotification = ({title, message, afterClose, hideModal}) => {
    const onClose = () => {
        hideModal();

        if (afterClose) {
            afterClose();
        }
    };

    return (
        <Modal onClose={onClose}>
            <div className="modal-header">
                <h1 className="modal-title">{title}</h1>
            </div>
            <div className="modal-body">
                <p>
                    {message}
                </p>
            </div>
            <div className="modal-footer">
                <button className="btn btn-white" onClick={onClose}>Close</button>
            </div>
        </Modal>
    );
};

export default connect(null, {hideModal})(ModalNotification);
