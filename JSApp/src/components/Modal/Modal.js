import React from 'react';

export default class Modal extends React.Component {
    constructor(props) {
        super(props);
    }

    listenKeyboard(event) {
        if (event.key === 'Escape' || event.keyCode === 27) {
            this.props.onClose();
        }
    }

    componentDidMount() {
        if (this.props.onClose) {
            window.addEventListener('keydown', this.listenKeyboard.bind(this), true);
        }
        if (!document.body.classList.contains('modal-open')) {
            document.body.classList.add('modal-open');
        }
    }

    componentWillUnmount() {
        if (this.props.onClose) {
            window.removeEventListener('keydown', this.listenKeyboard.bind(this), true);
        }
        if (document.body.classList.contains('modal-open')) {
            document.body.classList.remove('modal-open');
        }
    }

    onOverlayClick() {
        this.props.onClose();
    }

    onDialogClick(event) {
        event.stopPropagation();
    }

    render() {
        return (
            <div>
                <div className="modal-backdrop in"/>
                <div id="myModal" className="modal inmodal in" role="dialog" aria-hidden="true"
                     style={{display: 'block'}} onClick={this.onOverlayClick.bind(this)}>
                    <div className="modal-dialog" onClick={this.onDialogClick.bind(this)}>
                        <div className="modal-content bounceInRight" style={{width: '500px', margin: '0 auto'}}>
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}