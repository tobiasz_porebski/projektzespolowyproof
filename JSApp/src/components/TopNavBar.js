import React from "react";

export default class TopNavBar extends React.Component {
    constructor(props) {
        super(props);
    }

    navbarMinimalize() {
        if(document.body.classList.contains('mini-navbar')) {
            document.body.classList.remove('mini-navbar');
        } else {
            document.body.classList.add('mini-navbar');
        }
    }

    render() {
        return (
            <div className="row border-bottom">
                <nav className="navbar navbar-static-top">
                    <div className="navbar-header">
                        <button className="navbar-minimalize minimalize-styl-2 btn btn-primary" onClick={this.navbarMinimalize.bind(this)}>
                            <i className="glyphicon glyphicon-menu-hamburger"/>
                        </button>
                        <form className="navbar-form-custom">
                            <div className="form-group">
                                <input type="text" placeholder="Search for something..."
                                       className="form-control" name="top-search" id="top-search">
                                </input>
                            </div>
                        </form>
                    </div>
                    <ul className="nav navbar-top-links navbar-right">
                        <li>
                            <span className="m-r-sm text-muted welcome-message">
                                Welcome to INSPINIA+ Admin Theme.
                            </span>
                        </li>
                    </ul>
                </nav>
            </div>
        );
    }
}