import React from 'react';
import {connect} from 'react-redux';
import Home from './Home';
import {MODAL_TYPE_NOTIFICATION} from '../constants/ModalTypes';
import {showModal} from '../actions/modal';

function mapStateToProps(state) {
    return {
        pathname: state.router.location.pathname,
        search: state.router.location.search,
        hash: state.router.location.hash
    };
}

function mapDispatchToProps(dispatch) {
    return {
        componentDidMount: () => dispatch({type: 'HOME_COMPONENT'}),
        showNotificationModal: () => dispatch(
            showModal(MODAL_TYPE_NOTIFICATION, {
                title: 'This is an awesome notification.',
                message: 'This is a sample message for an awesome notification.',
                afterClose: function () {
                    console.log('Close awesome notification.');
                }
            })
        )
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)