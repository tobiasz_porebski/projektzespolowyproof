import React from "react";
import {Link} from "react-router-dom";

export default class LeftNavBar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <nav className="navbar-default navbar-static-side" role="navigation">
                <div className="sidebar-collapse">
                    <ul className="nav metismenu" id="side-menu">
                        <li className="nav-header">
                            <div className="dropdown profile-element">
                                <span className="clear">
                                    <span className="block m-t-xs">
                                        <strong className="font-bold">{this.props.userName}</strong>
                                    </span>
                                    <span className="text-muted text-xs block">
                                        {this.props.position}
                                    </span>
                                </span>
                            </div>
                        </li>
                        <li className={this.props.homeClassName}>
                            <Link to="/">
                                <i className="glyphicon glyphicon-th-large"/>
                                <span className="nav-label">Home</span>
                            </Link>
                        </li>
                        <li className={this.props.clientsClassName}>
                            <Link to="/clients">
                                <i className="glyphicon glyphicon-user"/>
                                <span className="nav-label">Clients</span>
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}