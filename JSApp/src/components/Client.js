import React from "react";
import {Link} from "react-router-dom";

export default class Client extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
    }

    render() {
        return (
            <div>
                <h1>Client {this.props.match.params.clientId}</h1>
                <li><Link to={`${this.props.match.url}/services`}>Services</Link></li>
            </div>
        );
    }
}
