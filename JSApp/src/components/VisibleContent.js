import React from 'react'
import {connect} from 'react-redux'
import Content from "./Content";

function mapStateToProps(state) {
    return state;
}

export default connect(mapStateToProps)(Content)