import React from "react";

export default class Service extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <h1>Service {this.props.match.params.serviceId}</h1>
            </div>
        );
    }
}
