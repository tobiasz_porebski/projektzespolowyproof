import {connect} from "react-redux";
import TopNavBar from "./TopNavBar";

function mapStateToProps(state) {
    return {
        homeClassName: state.navBar.homeClassName,
        clientsClassName: state.navBar.clientsClassName,
    };
}

export default connect(mapStateToProps)(TopNavBar);
