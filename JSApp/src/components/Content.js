import React from 'react';

import routes from '../routes';
import LeftNavBar from './VisibleLeftNavBar'
import TopNavBar from './VisibleTopNavBar'
import ModalContainer from './Modal/ModalContainer'

export default class Content extends React.Component {
    constructor(props) {
        super(props);
        this.setBodyClasses();
    }

    componentDidMount() {
        window.addEventListener('resize', () => this.setBodyClasses());
    }

    setBodyClasses() {
        if (window.innerWidth < 785) {
            if (!document.body.classList.contains('body-small')) {
                document.body.classList.add('body-small');
            }
            if (!document.body.classList.contains('mini-navbar')) {
                document.body.classList.add('mini-navbar');
            }
        } else {
            if (document.body.classList.contains('body-small')) {
                document.body.classList.remove('body-small');
            }
            if (document.body.classList.contains('mini-navbar')) {
                document.body.classList.remove('mini-navbar');
            }
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize');
    }

    render() {
        return (
            <div id="wrapper">
                <LeftNavBar/>
                <div id="page-wrapper" className="gray-bg dashbard-1">
                    <TopNavBar/>
                    <div className="row">
                        <div className="wrapper wrapper-content">
                            {routes}
                        </div>
                    </div>
                </div>
                <ModalContainer/>
            </div>
        );
    }
}
