import {connect} from "react-redux";
import LeftNavBar from "./LeftNavBar";

function mapStateToProps(state) {
    return {
        homeClassName: state.navBar.homeClassName,
        clientsClassName: state.navBar.clientsClassName,
        userName: 'David Williams',
        position: 'Art Director'
    };
}

export default connect(mapStateToProps)(LeftNavBar);
