import React from "react";

export default class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.componentDidMount();
    }

    render() {
        return (
            <div>
                <button onClick={this.props.showNotificationModal}>
                    Show modal
                </button>
            </div>
        );
    }
}
