import React from 'react'
import { connect } from 'react-redux'
import Service from "./Service";

function mapStateToProps(state) {
    return {
        pathname: state.router.location.pathname,
        search: state.router.location.search,
        hash: state.router.location.hash,
    };
}

export default connect(mapStateToProps)(Service)