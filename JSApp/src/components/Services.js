import React from "react";
import {Link} from "react-router-dom";

export default class Services extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <h1>Services</h1>
                <li><Link to={`${this.props.match.url}/1`}>Service 1</Link></li>
                <li><Link to={`${this.props.match.url}/2`}>Service 2</Link></li>
                <li><Link to={`${this.props.match.url}/3`}>Service 3</Link></li>
                <li><Link to={`${this.props.match.url}/4`}>Service 4</Link></li>
            </div>
        );
    }
}
