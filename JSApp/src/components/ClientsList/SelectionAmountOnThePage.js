import React from "react";
import $ from "jquery"

class SelectionAmountOnThePage extends React.Component {
    render() {
        return (
            <div className="dataTables_length">
                <label className="label-selection">Show
                    <select id="mySelect" className="form-control input-sm selection-info" onChange={() => {
                            this.props.changeSelectionAmountOnThePage($("#mySelect option:selected").val());
                            this.props.changePageHandler(1);
                            this.props.loadClients(1, $("#mySelect option:selected").val());
                        }
                    }>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select> clients
                </label>
            </div>
        )
    }
}
export default SelectionAmountOnThePage