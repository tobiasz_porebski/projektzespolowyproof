import React from 'react';

import Modal from '../Modal/Modal';

const DeleteConfirmation = (props) => {
    return (
        <Modal onCancel={props.onClose}
               isOpen={props.isOpen}>
            <div className="modal-body">
                <h2>Customer will be deleted. Are you sure you want to continue?</h2>
                <div style={{textAlign: 'center'}}>
                    <button onClick={props.onClose} className="btn btn-w-m btn-warning">Cancel</button>
                    <button onClick={props.onDelete} style={{marginLeft: "30px"}} className="btn btn-w-m btn-danger">Delete</button>
                </div>

            </div>
        </Modal>
    )
}

export default DeleteConfirmation;