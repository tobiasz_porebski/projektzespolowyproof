import React from 'react'
import ButtonAdd from "../../Buttons/ButtonAdd"
import ClientsMenu from "./Client"
import Pages from "./Pages";
import {Link} from 'react-router-dom';
import ClientsListLayout from './ClientsListLayout';
import {connect} from 'react-redux';

import actionTypes from '../../actions/actionTypes';
import DeleteConfirmation from './DeleteConfirmation';
import loadClientsActionCreator from '../../actions/clientsList';
import deleteClientActionCreator from '../../actions/deleteClient';
import loadClient from '../../actions/loadClient';
import {showModal} from '../../actions/modal';
import {MODAL_TYPE_NEW_CLIENT} from '../../constants/ModalTypes';
import SelectionAmountOnThePage from "./SelectionAmountOnThePage";

class ListClients extends React.Component {

    componentDidMount() {
        this.props.clientsComponentDidMount();
        this.props.loadClients(this.props.page, this.props.selectionAmountOnThePage);
    }

    render() {
        let List = Object.keys(this.props.clients).map((id) => {
            return <ClientsMenu onEditButtonHandler={this.props.loadClient}
                                deleteButtonHandler={this.props.showDeleteConfirmationHandler}
                                manageButtonHandler={this.props.showCSManagerHandler}
                                key={id}
                                id={parseInt(id)}
                                firstName={this.props.clients[id].firstName}
                                lastName={this.props.clients[id].lastName}
                                email={this.props.clients[id].email}
                                numberOfServices={this.props.clients[id].amountServices}/>
        });
        if (this.props.count === 0) {
            List.push(
                <div className="client-name">
                    <h2>No matching clients found</h2>
                </div>
            )
        }
        let deleteConfirmation = null;
        if (this.props.showDeleteConfirmation === true) {
            deleteConfirmation = <DeleteConfirmation key="deleteConfirmation"
                                                     isOpen={true}
                                                     onClose={this.props.closeDeleteConfirmationHandler}
                                                     onDelete={() => this.props.deleteClientHandler(this.props.deletingId)}/>
        }
        return (
            <ClientsListLayout>
                <div className="table-responsive">
                    <div className="dataTables_wrapper form-inline dt-bootstrap">
                        <div className="dataTables_info select-view">
                            <ButtonAdd onClick={this.props.showNewClientHandler}/>
                            <div className="select-block">
                                <SelectionAmountOnThePage key="selection"
                                                          changeSelectionAmountOnThePage={this.props.changeSelectionAmountOnThePage}
                                                          changePageHandler={this.props.changePageHandler}
                                                          loadClients={this.props.loadClients}/>
                            </div>
                        </div>
                        <div className="contact-list">
                            {List}
                            <Pages key="pages"
                                   amountPages={Math.ceil(this.props.count / this.props.selectionAmountOnThePage)}
                                   changePageHandler={this.props.changePageHandler}
                                   currentPage={this.props.page}
                                   loadClients={this.props.loadClients}
                                   amountOnThePage={this.props.selectionAmountOnThePage}/>
                            {deleteConfirmation}
                        </div>
                    </div>
                </div>
            </ClientsListLayout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        count: state.clientsList.count,
        clients: state.clientsList.clients,
        page: state.clientsList.currentPage,
        showNewClientModal: state.clientsList.showNewClientModal,
        showCSManager: state.clientsList.showCSManager,
        showDeleteConfirmation: state.clientsList.showDeleteConfirmation,
        deletingId: state.clientsList.deletingIdClient,
        selectionAmountOnThePage: state.clientsList.selectionAmountOnThePage,
        modal: state.modal
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadClients: (page, amountOnThePage) => dispatch(loadClientsActionCreator(page - 1, amountOnThePage)),
        changePageHandler: (pageNumber) => dispatch({type: actionTypes.CHANGE_PAGE, pageNumber: pageNumber}),
        showNewClientHandler: () => dispatch(showModal(MODAL_TYPE_NEW_CLIENT, {})),
        showCSManagerHandler: (id) => dispatch({type: actionTypes.SHOW_CLIENT_SERVICES_MANAGER, deletingId: id}),
        closeCSManagerHandler: () => dispatch({type: actionTypes.CLOSE_CLIENT_SERVICES_MANAGER}),
        showDeleteConfirmationHandler: (id) => dispatch({type: actionTypes.SHOW_DELETE_CONFIRMATION, deletingId: id}),
        closeDeleteConfirmationHandler: () => dispatch({type: actionTypes.CLOSE_DELETE_CONFIRMATION}),
        deleteClientHandler: (id) => dispatch(deleteClientActionCreator(id)),
        changeSelectionAmountOnThePage: (amount) => dispatch({
            type: actionTypes.CHANGE_SELECTION,
            selectionAmountOnThePage: amount
        }),
        loadClient: (id) => dispatch(loadClient(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListClients)