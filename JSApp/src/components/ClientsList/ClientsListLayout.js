import React from 'react';

const ClientsListLayout = (props) => {
    return(
        <div className="wrapper wrapper-content animated fadeInRight">
            <div className="row">
                <div className="col-lg-12">
                    <div className="ibox float-e-margins">
                        <div className="wrapper animated fadeInRight">
                            <div className="jumbotron">
                                <div><h1 className="client-name">Clients</h1></div>
                            </div>
                        </div>
                        <div className="wrapper animated fadeInRight">
                            <div className="jumbotron">
                                {props.children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ClientsListLayout;