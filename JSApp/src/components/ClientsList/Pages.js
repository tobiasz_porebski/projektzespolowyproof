import React from "react";
import PaginationLink from './PaginationLink';

class Pages extends React.Component {

    render() {
        if (this.props.amountPages !== 0) {
            let pageLinkList = [...Array(this.props.amountPages)].map((el, index) => <PaginationLink key={index}
                                                                                                     changePageHandler={this.props.changePageHandler}
                                                                                                     name={(index + 1).toString()}
                                                                                                     number={index + 1}
                                                                                                     currentPage={this.props.currentPage}
                                                                                                     maxPage={this.props.amountPages}
                                                                                                     loadClients={this.props.loadClients}
                                                                                                     amountOnThePage={this.props.amountOnThePage}/>
            )
            return (
                <div className="dataTables_paginate paging_simple_numbers anotherTools">
                    <ul className="pagination">
                        <PaginationLink key="previous"
                                        changePageHandler={this.props.changePageHandler}
                                        name={'Previous'}
                                        number={this.props.currentPage - 1}
                                        currentPage={this.props.currentPage}
                                        maxPage={this.props.amountPages}
                                        loadClients={this.props.loadClients}
                                        amountOnThePage={this.props.amountOnThePage}/>
                        {pageLinkList}
                        <PaginationLink key="next"
                                        changePageHandler={this.props.changePageHandler}
                                        name={'Next'}
                                        number={this.props.currentPage + 1}
                                        currentPage={this.props.currentPage}
                                        maxPage={this.props.amountPages}
                                        loadClients={this.props.loadClients}
                                        amountOnThePage={this.props.amountOnThePage}/>
                    </ul>
                </div>
            )
        }
        else {
            return (null)
        }
    }
}

export default Pages