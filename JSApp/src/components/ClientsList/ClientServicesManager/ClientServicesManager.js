import React from "react";
import React from 'react';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom'

import Modal from '../Modal';
import actionTypes from '../../actions/actionTypes';
import Cont from '../Cont';

const renderField = ({input, label, type, meta: {touched, error, warning}}) => (
    <div className="form-group">
        <div className="col-lg-10">
            <label>{label}</label>
            <div>
                <input className="form-control" {...input} placeholder={label} type={type}/>
                {touched && ((error && <span style={{color: "red"}}>{error}</span>) || (warning &&
                    <span>{warning}</span>))}
            </div>
        </div>
    </div>
)

const ClientForm = (props) => {
    //const {handleSubmit, pristine, reset, submitting} = props
    let redirect = null;
    if (props.showModal === false) {
        redirect = <Redirect to={'/clients'}/>
    }


}

class ClientServicesMenu extends React.Component {

    render()
    {
        return (
            <Cont>
                {redirect}
            </Cont>
        )
    }
}
/*
            <Modal isOpen={props.showModal} onCancel={props.closeModalHandler}>
                <div className="modal-body">
                    <h2>Add new client</h2>
                    <form className="form-horizontal" onSubmit={handleSubmit}>
                        <Field name="firstName" type="text"
                               component={renderField} label="Imię"
                               validate={[v.required, v.maxLength15]}
                        />
                        <div>
                            <button className="btn btn-primary btn-sm"
                                    type="button"
                                    disabled={pristine || submitting}
                                    onClick={reset}>Clear Values
                            </button>
                        </div>
                    </form>
                </div>
            </Modal>
*/