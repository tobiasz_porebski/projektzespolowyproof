import React from "react";
import ButtonEdit from '../../Buttons/ButtonEdit';
import ButtonManage from '../../Buttons/ButtonManage';
import ButtonDelete from '../../Buttons/ButtonDelete';
import Cont from '../Cont';
import {connect} from 'react-redux';
import updateClient from '../../actions/updateClient';

class ClientsMenu extends React.Component {

    render() {

        return (
            <Cont>
                <div className="ibox float-e-margins divTableRow">
                    <div className="ibox-title">
                        <h3 className="client-name">
                            <a>{this.props.firstName} {this.props.lastName}</a>
                        </h3>
                    </div>
                    <div className="ibox-content">
                        <div className="contact-info">
                            <div className="contact-info">
                                <span className="line-info"><strong>Email</strong>: {this.props.email}</span>
                                <span
                                    className="line-info"><strong>Active Services</strong>: {this.props.numberOfServices}</span>
                            </div>
                        </div>
                        <div className="buttonContainer">
                            <ButtonEdit onClick={() => this.props.onEditButtonHandler(this.props.id) }/>
                            <ButtonManage onClick={() => this.props.manageButtonHandler(this.props.id)}/>
                            <ButtonDelete onClick={() => this.props.deleteButtonHandler(this.props.id)}/>
                        </div>
                    </div>
                </div>
            </Cont>)
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateClientData: () => dispatch(updateClient(id, newData))
    }
}

export default connect(null, mapDispatchToProps)(ClientsMenu)