import React from 'react';

class PaginationLink extends React.Component {

    render() {
        if (this.props.currentPage === this.props.number
            || (this.props.currentPage === 1 && this.props.name === 'Previous')
            || (this.props.currentPage === this.props.maxPage && this.props.name === 'Next')) {
            return (
                <li className="paginate_button active">
                    <a>{this.props.name}</a>
                </li>
            )
        }
        else {
            return (
                <li className="paginate_button"
                    onClick={() => {
                        this.props.changePageHandler(this.props.number);
                        this.props.loadClients(this.props.number, this.props.amountOnThePage)
                    }
                    }>
                    <a>{this.props.name}</a>
                </li>
            )
        }
    }
}

export default PaginationLink;