import React from 'react'
import {Route, Switch} from "react-router-dom";
import VisibleHome from '../components/VisibleHome'
import VisibleClients from '../components/VisibleClients'
import VisibleClient from '../components/VisibleClient'
import VisibleServices from '../components/VisibleServices'
import VisibleService from '../components/VisibleService'
import fetchTest from '../fetchTest'
import ClientForm from '../components/NewClient/ClientForm'

const routes = (
    <div>
        <Route exact path="/" component={fetchTest}/>
        <Switch>
            <Route exact path="/" component={VisibleHome}/>
            <Route exact path="/clients" component={VisibleClients}/>
            <Route exact path="/clients/:clientId" component={VisibleClient}/>
            <Route exact path="/clients/:clientId/services" component={VisibleServices}/>
            <Route exact path="/clients/:clientId/services/:serviceId" component={VisibleService}/>
        </Switch>
    </div>
)

export default routes
