import {combineReducers} from 'redux';
import {reducer as reduxFormReducer} from 'redux-form';
import navBarReducer from './navBarReducer.js';
import clientsListReducer from './clientListReducer';
import clientsServiceManagerReducer from './clientServicesManagerReducer';
import clientsReducer from './newClientReducer';
import modalReducer from './modalReducer';

const rootReducer = combineReducers({
    form: reduxFormReducer,
    clients: clientsReducer,
    navBar: navBarReducer,
    clientsList: clientsListReducer,
    clientServiceManager: clientsServiceManagerReducer,
    modal: modalReducer
})

export default rootReducer;