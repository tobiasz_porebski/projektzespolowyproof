import actionTypes from '../actions/actionTypes';

const initialState = {
    currentPage: 1,
    showNewClientModal: false,
    showCSManager: false,
    showDeleteConfirmation: false,
    deletingIdClient: -1,
    count: 0,
    selectionAmountOnThePage: 5,
    clients: {},
    loadedClientData: {}
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CHANGE_SELECTION: {
            return {
                ...state,
                selectionAmountOnThePage: action.selectionAmountOnThePage
            }
        }
        case actionTypes.DISPLAY_CLIENTS_LIST: {
            return {
                ...state,
                clients: action.clientsData,
                count: action.count,
            };
        }
        case actionTypes.CHANGE_PAGE: {
            return {
                ...state,
                currentPage: action.pageNumber
            }
        }
        case actionTypes.SHOW_NEW_CLIENT_MODAL: {
            return {
                ...state,
                showNewClientModal: true
            }
        }
        case actionTypes.CLOSE_NEW_CLIENT_MODAL: {
            return {
                ...state,
                showNewClientModal: false
            }
        }
        case actionTypes.SHOW_CLIENT_SERVICES_MANAGER: {
            return {
                ...state,
                showCSManager: true,
                deletingIdClient: action.deletingId
            }
        }
        case actionTypes.CLOSE_CLIENT_SERVICES_MANAGER: {
            return {
                ...state,
                showCSManager: false,
                deletingIdClient: -1
            }
        }
        case actionTypes.SHOW_DELETE_CONFIRMATION: {
            return {
                ...state,
                showDeleteConfirmation: true,
                deletingIdClient: action.deletingId
            }
        }
        case actionTypes.CLOSE_DELETE_CONFIRMATION: {
            return {
                ...state,
                showDeleteConfirmation: false,
                deletingIdClient: -1
            }
        }
        case actionTypes.DELETE_CLIENT: {
            let clientsUpdated = {...state.clients}
            delete clientsUpdated[action.id]
            return {
                ...state,
                showDeleteConfirmation: false,
                deletingIdClient: -1,
                clients: clientsUpdated
            }
        }
        case actionTypes.LOAD_CLIENT: {
            return {
                ...state,
                loadedClientData: action.data
            }
        }
        default:
            return state;
    }
}

export default reducer;


/*[
        {
            "name": {
                "first": "Rivas",
                "last": "Huffman"
            },
            "contact": {
                "first": "Willis",
                "last": "Mccray"
            },
            "email": "Rivas.Huffman@gmail.com",
            "phone": "+1 (808) 414-3002",
            "address": "177 Channel Avenue, Riner, Florida, 1279"
        },
        {
            "name": {
                "first": "Constance",
                "last": "Pugh"
            },
            "contact": {
                "first": "Lindsay",
                "last": "Kline"
            },
            "email": "Constance.Pugh@gmail.com",
            "phone": "+1 (967) 451-3184",
            "address": "743 Gerry Street, Glenbrook, Tennessee, 7091"
        },
        {
            "name": {
                "first": "Rivas",
                "last": "Huffman"
            },
            "contact": {
                "first": "Willis",
                "last": "Mccray"
            },
            "email": "Rivas.Huffman@gmail.com",
            "phone": "+1 (808) 414-3002",
            "address": "177 Channel Avenue, Riner, Florida, 1279"
        },
        {
            "name": {
                "first": "Constance",
                "last": "Pugh"
            },
            "contact": {
                "first": "Lindsay",
                "last": "Kline"
            },
            "email": "Constance.Pugh@gmail.com",
            "phone": "+1 (967) 451-3184",
            "address": "743 Gerry Street, Glenbrook, Tennessee, 7091"
        },
        {
            "name": {
                "first": "Rivas",
                "last": "Huffman"
            },
            "contact": {
                "first": "Willis",
                "last": "Mccray"
            },
            "email": "Rivas.Huffman@gmail.com",
            "phone": "+1 (808) 414-3002",
            "address": "177 Channel Avenue, Riner, Florida, 1279"
        },
        {
            "name": {
                "first": "Constance",
                "last": "Pugh"
            },
            "contact": {
                "first": "Lindsay",
                "last": "Kline"
            },
            "email": "Constance.Pugh@gmail.com",
            "phone": "+1 (967) 451-3184",
            "address": "743 Gerry Street, Glenbrook, Tennessee, 7091"
        }
    ]*/