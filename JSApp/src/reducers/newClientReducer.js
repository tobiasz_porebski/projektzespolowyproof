
const initialState = {
    addedClients: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_NEW_CLIENT": {
            console.log(action.data)
            return state;
        }
        default:
            return state;
    }
}

export default reducer;