import actionTypes from '../actions/actionTypes';

const initialState = {
    isShown: false,
    clientId: -1
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
}

export default reducer;