function navBarReducer(state = {}, action) {
    switch (action.type) {
        case "HOME_COMPONENT":
            return {homeClassName: "active", clientsClassName: ""};
        case "CLIENTS_COMPONENT":
            return {homeClassName: "", clientsClassName: "active"};
        default:
            return state;
    }
}

export default navBarReducer;